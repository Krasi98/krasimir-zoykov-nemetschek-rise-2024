const { fetchData } = require('./fetchData');

async function outputOrderStatuses() {
    try {
        const jsonData = await fetchData();
        const orders = jsonData.orders;
        const customers = jsonData.customers;

        console.log("Order Statuses:");
        orders.forEach(order => {

            const customer = customers.find((cust) => cust.id === order.customerId);
            const status = determineOrderStatus(order);
            console.log(`Order ID: ${order.customerId} for customer ${customer.name}, Status: ${status}`);
        });

        console.log('-----------------------------------------------------');

    } catch (error) {
        console.error("Error fetching order statuses:", error);
    }
}

function determineOrderStatus(order) {
    if (order.status === "Delivered") {
        return "Delivered";
    } else if (order.status === "In Transit") {
        return "In Transit";
    } else {
        return "Pending";
    }
}

module.exports = outputOrderStatuses;
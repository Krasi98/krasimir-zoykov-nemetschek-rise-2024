const fs = require('fs').promises;
const path = require('path');
const { setInterval } = require('timers/promises');

const dbJsonPath = path.join(__dirname, '..', 'data', 'db.json');

async function fetchData() {
    try {
        const data = await fs.readFile(dbJsonPath, { encoding: 'utf8' });

        const jsonData = JSON.parse(data);

        return jsonData;
    } catch (error) {
        console.error('Error reading db.json:', error);
        throw error;
    }
}

async function watchForUpdates() {
    try {
        let prevData = await fetchData();

        setInterval(async () => {
            const newData = await fetchData();

            if (!isEqual(prevData, newData)) {
                prevData = newData;
                console.log('Changes detected in db.json file.');
            } else  {
                console.log('NO changes detected in db.json file.');
            }
            
        }, 6000)
    } catch (error) {
        console.error('Error watching for updates:', error);
    
    }
}



module.exports = {fetchData, watchForUpdates}
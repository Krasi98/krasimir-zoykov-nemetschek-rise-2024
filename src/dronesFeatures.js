const { fetchData } = require('./fetchData');

async function calculateBatteryLife() {
    const jsonData = await fetchData();
    const typesOfDrones = jsonData.typesOfDrones;
    const descriptionBattery = []
    const consumationOfBattery = [];
    const processingTypeDrones = [];

    typesOfDrones.forEach((dron) => {

        const resultConsPerMin = batteryConsumationPerKgPerMin(dron.capacity, dron.consumption, dron.type);
        processingTypeDrones.push(resultConsPerMin);

        let batteryLife;
        if (dron.capacity.includes('kW') && dron.capacity.includes('W')) {
            const capacityInKw = Number(dron.capacity.split('kW').join('')) * 1000;
            const consumptionInW = Number(dron.consumption.split("W").join(''));
            batteryLife = capacityInKw / consumptionInW;
            consumationOfBattery.push(batteryLife);


        } else if (dron.capacity.includes('W') && dron.capacity.includes('W')) {
            const capacityInW = Number(dron.capacity.split('W').join('')) / 100 * 100;
            const consumptionInW = Number(dron.consumption.split("W").join(''));
            batteryLife = capacityInW / consumptionInW;
            consumationOfBattery.push(batteryLife);
        }

        const hours = Math.floor(batteryLife / 60);
        const minutes = Math.floor(batteryLife % 60);

        descriptionBattery.push(`Battery Life for drone with capacity ${dron.capacity} and consumption ${dron.consumption}: ${hours} hours ${minutes} minutes`);
    });

    return { descriptionBattery, consumationOfBattery, processingTypeDrones };
}


function batteryConsumationPerKgPerMin(capacity, consumation, type) {

    let capacityInWatts;
    const consumationOfBatteryPerMin = [];

    if (capacity.includes('kW')) {
        capacityInWatts = Number(capacity.replace('kW', "")) * 1000;
    } else if (capacity.includes('W')) {
        capacityInWatts = Number(capacity.replace("W", ""));
    }

    const consumationInWatts = Number(consumation.replace("W", ""));

    const consumationPerKgPerMin = consumationInWatts / capacityInWatts;

    consumationOfBatteryPerMin.push(`Drone: ${capacity} | Consumption: ${consumation} | Consumption per kg per min: ${consumationPerKgPerMin} W/kg/min`);
   
    return consumationOfBatteryPerMin;
}

module.exports = {calculateBatteryLife} ;
const calculateDeliveryTime = require('./calculateDeliveryTime');

async function calculateAverageDeliveryTime() {
    
    const { totalTime, countOfDrones } = await calculateDeliveryTime();

    const averageDeliveryTime = totalTime / countOfDrones;


    const hours = Math.floor(averageDeliveryTime / 60);
    const minutes = Math.round(averageDeliveryTime % 60);

    const hoursAndMunites = `Convert average delivery time: ${hours} hours and ${minutes} minutes.`
    return {averageDeliveryTime, hoursAndMunites};
}

module.exports = calculateAverageDeliveryTime;
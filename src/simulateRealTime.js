const { fetchData } = require('./fetchData');
const { printResult } = require('./printResultOne');
const printResultTwo = require('./printResutTwo');
const outputOrderStatuses = require('./outputOrderStatuses');

async function main() {
    try {
        const jsonData = await fetchData();
        const output = jsonData.output;

        if (output.poweredOn) {
            const programMinutes = output.minutes.program;
            const realMilliseconds = output.minutes.real;
            const programMilliseconds = programMinutes * 60 * 1000;

            await printResult();
            await printResultTwo();
            await simulateRealTime(programMilliseconds, realMilliseconds);

            if (output.orderStatusOutputEnabled) {
                const statusOutputFrequency = output.statusOutputFrequency;
                const frequencyInMilliseconds = statusOutputFrequency * 60 * 1000;
                setInterval(outputOrderStatuses, frequencyInMilliseconds);
                // await outputOrderStatuses();
            } else {
                console.log("Order status output is not enabled. Skipping...")
            }

        } else {
            console.log("Program is not powered on. Exiting...");
        }
    } catch (error) {
        console.error("Error from main:", error);
        throw error;
    }
}

async function simulateRealTime(programMilliseconds, realMilliseconds) {
    return new Promise((resolve) => {
        setTimeout(resolve, realMilliseconds - programMilliseconds);
    });
}


module.exports = main;
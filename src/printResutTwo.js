const { calculateBatteryLife } = require('./dronesFeatures');
const calculateTotalWeight = require('./calculateTotalWeight');
const chargingStationsNetwork = require('./chargingStationsNetwork');

async function printResultTwo() {

    const {descriptionBattery, consumationOfBattery, processingTypeDrones} = await calculateBatteryLife();
    const resultToPrint = await calculateTotalWeight();
    const stationLocationForDrones = await chargingStationsNetwork();

    setTimeout(() => {
        console.log(resultToPrint.join('\n'));
        console.log('----------------------------------------------------')
    }, 7000)

    setTimeout(() => {
        console.log(processingTypeDrones.join('\n'));
        console.log('----------------------------------------------------')
    }, 8000);


    setTimeout(() => {
        console.log(stationLocationForDrones.join('\n'));
        console.log('----------------------------------------------------')
    }, 9000);
}

module.exports = printResultTwo;
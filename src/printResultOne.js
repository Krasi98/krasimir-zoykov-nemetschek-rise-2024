const calculateDeliveryTime = require('./calculateDeliveryTime');
const { calculateBatteryLife } = require('./dronesFeatures');
const calculateAverageDeliveryTime = require('./calculateAverageDeliveryTime');


async function printResult() {

    const printCountOfDrones = [];
    const printDroneTypes = [];
    const dronesRecharg = [];
    let printTotalTime = '';


    const { totalTime, countOfDrones, distanceCustomersResult, totalDistance } = await calculateDeliveryTime();
    const { descriptionBattery, consumationOfBattery } = await calculateBatteryLife();
    const { averageDeliveryTime, hoursAndMunites } = await calculateAverageDeliveryTime();


    printDroneTypes.push(descriptionBattery.join('\n'));
 

    if (countOfDrones > 1) {
        printCountOfDrones.push(`To deliver all orders we need from ${countOfDrones} drones`);
    } else {
        printCountOfDrones.push(`To deliver all orders we need from ${countOfDrones} drone`);
    }


    const deliveryTimeForDrones = [];
    const dronesToRecharge = [];

    consumationOfBattery.forEach((consumption, index) => {
        let totalDeliveryTime = 0;
        totalDistance.forEach((distance) => {
            const timeForDelivery = distance / consumption;
            totalDeliveryTime += timeForDelivery;
        });
        deliveryTimeForDrones.push(totalDeliveryTime);
    })

    deliveryTimeForDrones.forEach((time, index) => {

        if (time > consumationOfBattery[index]) {
            dronesToRecharge.push(index + 1);
        }
    })

    if (dronesToRecharge.length > 0) {
        totalTime += dronesToRecharge.length * 20;
        dronesRecharg.push(`The following drones need to be recharged: ${dronesToRecharge.join(', ')}`)
    } else {
        dronesRecharg.push("All drones can complete the deliveries without recharging.")
    }

    printTotalTime = "The total time needed to deliver all orders " + totalTime.toFixed(2) + ' minutes';
   
    simulateRetardation(distanceCustomersResult, printTotalTime, printCountOfDrones, printDroneTypes, dronesRecharg, averageDeliveryTime, hoursAndMunites);
}



function simulateRetardation(distanceCustomersResult, printTotalTime, printCountOfDrones, printDroneTypes, dronesRecharg, averageDeliveryTime, hoursAndMunites) {

    setTimeout(() => {
        Object.values(distanceCustomersResult).forEach((result) => {
            console.log(result);
        });
        console.log('----------------------------------------------------');
    }, 1000);

    setTimeout(() => {
        console.log(printTotalTime);
        console.log('----------------------------------------------------');
    }, 2000);

    setTimeout(() => {
        printCountOfDrones.forEach((result) => {
            console.log(result);
            console.log('----------------------------------------------------');
        })
    }, 3000);


    setTimeout(() => {
        printDroneTypes.forEach((result) => {
            console.log(result);
            console.log('----------------------------------------------------');
        })
    }, 4000);


    setTimeout(() => {
        dronesRecharg.forEach((result) => {
            console.log(result);
            console.log('----------------------------------------------------');
        })
    }, 5000);

    const sunnyDayDurationMinutes = 12 * 60

    setTimeout(() => {
        console.log(`The average delivery time for all orders is: ${averageDeliveryTime.toFixed(2)} minutes`);
        console.log(hoursAndMunites);
        if (averageDeliveryTime > sunnyDayDurationMinutes) {
            console.log("We cannot complete all deliveries in one sunny day.");
        } else {
            console.log("All deliveries can be completed within one sunny day.");
        }
        console.log('----------------------------------------------------');
    }, 6000);
}

module.exports = { printResult, simulateRetardation };
const { fetchData, watchForUpdates } = require('./fetchData');

async function calculateTotalWeight() {

    const jsonData = await fetchData();
    const orders = jsonData.orders;
    const products = jsonData.products;

    let checkForProducts = '';
    let checkForWeight = '';

    let totalWeightFromOrders = 0;
    let totalWeightFromProducts = 0;

    const productWeigthOrdersInGrams = {};
    const productWeigthProductsInGrams = {};

    const resultToPrint = [];

    orders.forEach((order) => {
        const productList = order.productList;
        Object.entries(productList).forEach(([productType, quantity], index) => {

            if (!products[productType]) {
                checkForProducts = `Product "${productType}" is missing from the products configuration.`
            }

            const weightGrams = quantity * 100;
            totalWeightFromOrders += weightGrams;

            if (!productWeigthOrdersInGrams.hasOwnProperty(productType)) {
                productWeigthOrdersInGrams[productType] = weightGrams
            }

            checkForProducts = "All necessary products are in list."

        });
    });


    Object.entries(products).forEach(([productType, quantity]) => {
        const weightForGrams = quantity * 100;

        if (!productWeigthProductsInGrams.hasOwnProperty(productType)) {
            productWeigthProductsInGrams[productType] = weightForGrams;
        }
        totalWeightFromProducts += weightForGrams;
    });



    Object.entries(productWeigthOrdersInGrams).forEach(([productOrders, quantityOrders]) => {
        Object.entries(productWeigthProductsInGrams).forEach(([productProducts, quantityProducts]) => {
            if (productOrders === productProducts) {
                if (quantityOrders > quantityProducts) {
                    checkForWeight = "We don't have enough products to make all orders";
                } else {
                    checkForWeight = "We have enough products to make all orders";
                }
            }
        });
    });

    resultToPrint.push(`Total weight for oreders ${totalWeightFromOrders} grams.`);
    resultToPrint.push(`Total weight for all products ${totalWeightFromProducts} grams.`);
    resultToPrint.push(checkForWeight);
    resultToPrint.push(checkForProducts);
   
    return resultToPrint;

}


module.exports = calculateTotalWeight;
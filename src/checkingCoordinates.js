const { fetchData } = require('./fetchData');

async function dispatchCustomerCoordinates() {
    const jsonData = await fetchData();
    const customers = jsonData.customers;
    const mapCoordinate = jsonData.customers

    try {
        checkCustomerCoordinates(customers, mapCoordinate);
        console.log("All customer coordinates are valid.");
        console.log("---------------------------------------------");

    } catch (error) {
        console.error("Error checking customer coordinates:", error.message);
        throw error
    }
}


function checkCustomerCoordinates(customers, mapCoordinate) {
    customers.forEach((customer) => {
        const x = Number(customer.coordinates.x);
        const y = Number(customer.coordinates.y);

        if (isNaN(x) || isNaN(y) || x < 0 || y < 0 || x > mapCoordinate.x || y > mapCoordinate.y) {
            throw new Error(`Customer coordinates (${x}, ${y}) are outside the map boundaries or invalid.`);
        }
    });
}


module.exports = dispatchCustomerCoordinates;
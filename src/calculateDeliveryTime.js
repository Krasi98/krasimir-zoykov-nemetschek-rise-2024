const { fetchData } = require('./fetchData');

async function calculateDeliveryTime() {
    const jsonData = await fetchData();
    let totalTime = 0;
    let countOfDrones = 0;
    const totalDistance = []

    const customers = jsonData.customers;
    const orders = jsonData.orders;
    const warehouses = jsonData.warehouses
    let waitingTimeAtWarehouse = orders.length * 5;

    const distanceCustomersResult = {};

    orders.forEach((order) => {
        countOfDrones++;

        const customer = customers.find((cust) => cust.id === order.customerId);

        const distance = calculateDistance(customer, warehouses)
        totalTime += waitingTimeAtWarehouse + distance;


        if (!distanceCustomersResult[customer.id]) {
            distanceCustomersResult[customer.id] = `Distance from ${customer.name} to all warehouses: ${distance.toFixed(2)} minutes`;
        }

        totalDistance.push(distance);
    });

    return { totalTime, countOfDrones, distanceCustomersResult, totalDistance }
}

function calculateDistance(customer, warehouses) {
    let totalDistance = 0;

    warehouses.forEach((warehouse) => {
        const x = warehouse.x - customer.coordinates.x;
        const y = warehouse.y - customer.coordinates.y;
        const distance = Math.sqrt(x * x + y * y);
        totalDistance += distance;
    });

    return totalDistance;
}

module.exports = calculateDeliveryTime;
const { fetchData } = require('./fetchData');

async function chargingStationsNetwork() {

    const jsonData = await fetchData();

    const chargingStations = jsonData.chargingStations;
    const typesOfDrones = jsonData.typesOfDrones;

    const stationLocationForDrones = [];


    chargingStations.forEach((station, index) => {
        let stationFoundForDrone = false;
        typesOfDrones.forEach((drone, index) => {
            if (drone.type.includes(station.type)) {
                const { x, y } = station;
                stationLocationForDrones.push(`The location of the station type ${station.type} for drone type ${drone.type} is x ${x} and y ${y}`)
                stationFoundForDrone = true;
            }
        });

        if (!stationFoundForDrone) {
            stationLocationForDrones.push(`No suitable charging station found for station type ${station.type}`);
        }
    });

    return stationLocationForDrones;

}

module.exports = chargingStationsNetwork;
const {fetchData, watchForUpdates} = require('./src/fetchData');
const dispatchCustomerCoordinates = require('./src/checkingCoordinates');
const main = require('./src/simulateRealTime');

main();
dispatchCustomerCoordinates();
watchForUpdates();